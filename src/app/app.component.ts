
import { Component } from '@angular/core';
import { AngularFireDatabase} from 'angularfire2/database';
import { AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}