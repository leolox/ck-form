import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';

import { FormComponent } from './components/form/form.component';
import { VideoUploadComponent } from './components/video-upload/video-upload.component';
import { SubmittedComponent } from './components/form/submitted/submitted.component';
import { LoginComponent } from './components/admin/login/login.component';
import { VoteComponent } from './components/admin/jury/vote/vote.component';

const routes: Routes = [
  { path: '', component: FormComponent },
  { path: 'finish', component: SubmittedComponent },
  { path: 'upload', component: VideoUploadComponent },

  //admin paths
  { path: 'login', component: LoginComponent },
  
  { path: 'jury/vote',
    component: VoteComponent,
    canActivate: [AngularFireAuthGuard]
  },

  { path: '**', component: FormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
