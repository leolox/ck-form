import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil, map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-video-upload',
  templateUrl: './video-upload.component.html',
  styleUrls: ['./video-upload.component.scss']
})
export class VideoUploadComponent implements OnInit {
  ngUnsubscribe: Subject<any> = new Subject<any>();

  private filmsCollection: AngularFirestoreCollection<any>;
  private filmDocument: AngularFirestoreDocument<any>;
  
  constructor(
    private router: Router,
    private afs: AngularFirestore,
    private activatedRoute: ActivatedRoute,
  ) { }

  docID: string;
  linkValid= false;
  loading = true;
  ngOnInit() {
    this.activatedRoute.queryParams
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe(params => {
      console.log(params);

      if ((params.vid == undefined)){
        this.linkValid = false;
      }else{
        this.docID = params.vid;
      }

    });

    //set document
    this.filmDocument = this.afs.doc('forms/' + this.docID);
    this.DocumentExists();

  }


  DocumentExists(){
    this.filmDocument.update({uploadPortalOpen: true})
    .then(response => {      
      this.linkValid = true;
      this.loading = false;
    })
    .catch(response => {
      //console.log(response);
      this.linkValid = false;
      this.loading = false;
    })

  }

  updateDocument(data: any){
    this.filmDocument.update(data)
    .then(()=> {
      return true;
    }).catch(()=> {
      return false;
    })
  }

  userURL: string = "";
  userNotes: string = "";
  formSucess: boolean;
  onSubmit(){
    console.log(this.userURL, this.userNotes)
    this.filmDocument.update({ uploadUserURL: this.userURL, uploadUserNotes: this.userNotes })
    .then(()=>{
      this.formSucess = true;
    })
    .catch(()=> {
      this.formSucess = false;
    })
  }

}