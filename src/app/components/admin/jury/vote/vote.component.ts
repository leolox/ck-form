import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { map } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss'],
  
})
export class VoteComponent implements OnInit, OnDestroy {

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    public afAuth: AngularFireAuth,
  ) { }

  subscriptions: Subscription[] = [];

  formsData;
  currentSelection: number;
  videoURL;


  ngOnInit(): void {
    this.currentSelection = parseInt( localStorage.getItem("currentSelection")) || 0;
    this.getForms();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  getForms(){
    var formsColl = this.afs.collection<any>('forms');
    var forms = formsColl.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as any;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );

    this.subscriptions.push(
      forms.subscribe(forms => {   
        if(forms != null){
          this.formsData = forms;
          console.log(this.formsData)

          //select
          this.onFormSelect(this.currentSelection, this.formsData[this.currentSelection]);

        }   
      }))
  }

  onFormSelect(index, form){
    this.currentSelection = index;
    this.getVideoURL(form.latestUpload);
    localStorage.setItem("currentSelection", index);
    this.checkAndGetVote(form.id);

    try{
      this.DBVoteSub.unsubscribe();
     }catch{}
    
  }

  onNext(){
    console.log(this.formsData.length)
    if(this.currentSelection < this.formsData.length -1){
      var newIndex= this.currentSelection + 1;
      this.onFormSelect(newIndex, this.formsData[newIndex]);
    }else{
      this.snackBar.open("nope", "", {
        duration:  2*1000,
      });
    }

  }
  onPrevious(){
    if(this.currentSelection > 0){
      var newIndex= this.currentSelection - 1;
      this.onFormSelect(newIndex, this.formsData[newIndex]);
    }else{
      this.snackBar.open("nope", "", {
        duration:  2*1000,
      });
    }
 
  }

  videoRdy = false;
  getVideoURL(path){
    this.videoRdy = false;
    console.log(path)
    if(path){
      const ref = this.storage.ref(path);
      var test = ref.getDownloadURL();
      
      var urlSub= test.subscribe((url)=>{
        this.videoURL = url;
        console.log(url);
        this.videoRdy = true;
        urlSub.unsubscribe();
      })
    }
    else{
      this.snackBar.open("video nicht hochgeladen check mal die links ab", "", {
        duration:  5*1000,
      });
    }

  }

  judgeID = this.afAuth.auth.currentUser.email;
  onVoteSubmit(form ,id){
    console.log(form);
    console.log(id);
    console.log(this.judgeID);

    var value = form.form.value
    var validator = value.drehbuch < 101 && value.acting < 101 && value.bildgestalgung < 101 && value.audio < 101 && value.postpruduktion <101 && value.gesamteindruck < 101;

    if(form.form.status != "INVALID" && validator){

      var data = {
        drehbuch: value.drehbuch,
        acting: value.acting,
        bildgestalgung: value.bildgestalgung,
        audio: value.audio,
        postpruduktion: value.postpruduktion,
        gesamteindruck: value.gesamteindruck,
        question1: value.question1,
        question2: value.question2
      }

      var voteColl = this.afs.collection<any>(`/forms/${id}/judges/`);
      
      
      voteColl.doc(this.judgeID).set(data, {merge: true})
      .then(()=> {
  
        this.snackBar.open("Eine unglaubliche Bewertung", "👾", {
          duration:  2*1000,
        });
        form.form.reset();
        this.checkAndGetVote(id);

      }).catch(()=> {
  
        this.snackBar.open("Oooch nee, da ist wohl was schief gelaufen.", "🐞", {
          duration:  6*1000,
        });
  
      })
      
      console.log(data);
    
    }else {
      this.snackBar.open("nope", "🤨", {
        duration:  3*1000,
      });
      
    }
     

  }
  
  //rateControl = new FormControl("", [Validators.max(100), Validators.min(0)])

  DBVote;
  DBVoteSub: Subscription;
  checkAndGetVote(id){
    var voteDoc = this.afs.doc(`/forms/${id}/judges/${this.judgeID}`);

    voteDoc.update( {}).then(()=> {
      //doc exists
      this.DBVoteSub = voteDoc.valueChanges().subscribe((vote)=> {
        this.DBVote = vote;
        console.log(this.DBVote)
        
      })


    }).catch(()=> {
      //no doc
      this.DBVote = null;
    })
 
  }

}
