import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {


  isSubmitted = false;
  firstName: string;
  private formsCollection: AngularFirestoreCollection<any>;

  constructor(
    private afs: AngularFirestore
  ) {
    this.formsCollection = afs.collection<any>('/forms/');
  }
  ngOnInit(): void {
  }

  onSubmit(data) {
    console.log(data)
    
    if(data.form.status !== "INVALID"){

      const now = new Date();
      
      this.firstName = data.form.value.YourDetails.vorname;

      var data = data.form.value;
      data.onSubmitTime = now;
      
      this.setDoc(data);

    }else{
      window.alert("Bitte Warnmeldung einführen")
    }
    

  }

  setDoc(data: any) {
    const id = this.afs.createId();
    const item: any = { id, name };
    this.formsCollection.doc(id).set(data)
    .then(()=> {
      localStorage.setItem('formID',JSON.stringify(id));
      this.isSubmitted = true;
    })
    .catch((err)=> {
      //console.log(err);
      window.alert("leider ist ein Fehler aufgetreten versuche es später nochmal.")
    });
  }

}