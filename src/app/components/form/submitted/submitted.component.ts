import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-submitted',
  templateUrl: './submitted.component.html',
  styleUrls: ['./submitted.component.scss']
})
export class SubmittedComponent implements OnInit, OnDestroy {

  @Input() firstName: string;

  //localStorage.setItem('formID',JSON.stringify(id));
  docID = JSON.parse(localStorage.getItem('formID'));

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    document.body.classList.add('page-wrapper-sucess');
  }
  ngOnDestroy(){
    document.body.classList.remove("page-wrapper-sucess")
  }

  uploadButton(){
    this.router.navigate(['/upload'], { queryParams: { vid: this.docID } });
  }

}
