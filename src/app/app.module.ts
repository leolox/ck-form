
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AngularFireModule } from 'angularfire2';
import {AngularFirestoreModule } from 'angularfire2/firestore';
import {AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import { AngularFireFunctionsModule } from '@angular/fire/functions';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FormComponent } from './components/form/form.component';
import { VideoUploadComponent } from './components/video-upload/video-upload.component';

import localeDe from '@angular/common/locales/de';
import { registerLocaleData } from '@angular/common';
import { SubmittedComponent } from './components/form/submitted/submitted.component';
import { DropzoneDirective } from './directives/dropzone.directive';
import { UploaderComponent } from './components/video-upload/uploader/uploader.component';
import { UploadTaskComponent } from './components/video-upload/upload-task/upload-task.component';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { LoginComponent } from './components/admin/login/login.component';
import { VoteComponent } from './components/admin/jury/vote/vote.component';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';


registerLocaleData(localeDe, 'de');

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    VideoUploadComponent,
    SubmittedComponent,
    DropzoneDirective,
    UploaderComponent,
    UploadTaskComponent,
    LoginComponent,
    VoteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    BrowserAnimationsModule,
    ScrollingModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,
    AngularFireAuthGuardModule,
    NgxAuthFirebaseUIModule.forRoot(
      environment.firebase,
      () => 'your_app_name_factory',
    {
      enableFirestoreSync: true, // enable/disable autosync users with firestore
      toastMessageOnAuthSuccess: false, // whether to open/show a snackbar message on auth success - default : true
      toastMessageOnAuthError: true, // whether to open/show a snackbar message on auth error - default : true
      authGuardFallbackURL: '/', // url for unauthenticated users - to use in combination with canActivate feature on a route
      authGuardLoggedInURL: '/loggedin', // url for authenticated users - to use in combination with canActivate feature on a route
      passwordMaxLength: 60, // `min/max` input parameters in components should be within this range.
      passwordMinLength: 6, // Password length min/max in forms independently of each componenet min/max.
      // Same as password but for the name
      nameMaxLength: 50,
      nameMinLength: 2,
      // If set, sign-in/up form is not available until email has been verified.
      // Plus protected routes are still protected even though user is connected.
      guardProtectedRoutesUntilEmailIsVerified: true,
      enableEmailVerification: true, // default: true
    })
    
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
